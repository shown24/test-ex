import React, {Component} from 'react'
import Card from "./conponents/Card";
import './App.css';

export class App extends Component {

  constructor() {
    super();
    this.state = {
      list: {},
      listApplied: [],
      listInterviewing: [],
      listHired: []
    };
  }

  getList() {
    fetch('https://randomuser.me/api/?nat=gb&results=5')
      .then(function (response) {
        return response.json();
      })
      .then((response) => {
        console.log(response.results);
        let list = {};
        let listApplied = [];
        response.results.forEach((el) => {
          list[el.id.value] = {
            name: `${el.name.first} ${el.name.last}`,
            picture: el.picture.thumbnail,
            city: el.location.city,
            status: 'applied'
          };
          listApplied.push(el.id.value);
        });
        this.setState({list, listApplied: this.sortList(listApplied, list)});
      });
  }

  sortList(arr, list) {
    let sortList = list ? list : this.state.list;
    console.log(arr)
    let newArr = arr.sort((a, b) => {
      if (sortList[a][this.getSort()] < sortList[b][this.getSort()]) {
        return -1;
      } else if (sortList[a][this.getSort()] > sortList[b][this.getSort()]) {
        return 1;
      } else {
        return 0;
      }
    });
    console.log(newArr)
    return newArr;
  }

  resortList() {
    this.setState({
      listApplied: this.sortList(this.state.listApplied),
      listInterviewing: this.sortList(this.state.listInterviewing),
      listHired: this.sortList(this.state.listHired)
    })
  }

  toAppliedList(el) {
    let newListInterviewing = this.state.listInterviewing.slice();
    if (newListInterviewing.indexOf(el) !== -1) {
      newListInterviewing.splice(newListInterviewing.indexOf(el), 1);
    }
    this.setState({
      listApplied: this.sortList([...this.state.listApplied, ...[el]]),
      listInterviewing: newListInterviewing
    });

  }

  toInterviewingList(el) {
    let newListApplied = this.state.listApplied.slice();
    let newListHired = this.state.listHired.slice();
    if (newListApplied.indexOf(el) !== -1) {
      newListApplied.splice(newListApplied.indexOf(el), 1);
    }
    if (newListHired.indexOf(el) !== -1) {
      newListHired.splice(newListHired.indexOf(el), 1);
    }
    this.setState({
      listApplied: newListApplied,
      listInterviewing: this.sortList([...this.state.listInterviewing, ...[el]]),
      listHired: newListHired
    });
  }

  toHiredList(el) {
    let newListInterviewing = this.state.listInterviewing.slice();
    if (newListInterviewing.indexOf(el) !== -1) {
      newListInterviewing.splice(newListInterviewing.indexOf(el), 1);
    }
    this.setState({
      listHired: this.sortList([...this.state.listHired, ...[el]]),
      listInterviewing: newListInterviewing
    });
  }

  setSort(str) {
    let storageSort = localStorage.getItem('baseSort');
    let sort = str ? str
      : storageSort ? storageSort : 'name';
    localStorage.setItem('baseSort', sort);
    this.resortList();
  }

  getSort() {
    return localStorage.getItem('baseSort')
  }

  componentDidMount() {
    this.setSort();
    this.getList();
  }

  render() {
    return (
      <div className="main-area">
        <div className="sort">
          <button onClick={() => this.setSort('name')}>Sort by Name</button>
          <button onClick={() => this.setSort('city')}>Sort by City</button>
        </div>
        <div className="stages">
          <div className="stage">Applied</div>
          <div className="stage">Interviewing</div>
          <div className="stage">Hired</div>
        </div>
        <div className="stages_list">
          <div className="stage-col">
            {this.state.listApplied.map((el) => <Card key={el}
                                                      {...(this.state.list[el])}
                                                      toRight={() => this.toInterviewingList(el)}/>)}
          </div>
          <div className="stage-col">
            {this.state.listInterviewing.map((el) => <Card key={el}
                                                           {...(this.state.list[el])}
                                                           toLeft={() => this.toAppliedList(el)}
                                                           toRight={() => this.toHiredList(el)}/>)}
          </div>
          <div className="stage-col">
            {this.state.listHired.map((el) => <Card key={el}
                                                    {...(this.state.list[el])}
                                                    toLeft={() => this.toInterviewingList(el)}/>)}
          </div>
        </div>
      </div>
    )
  }
}
