import React from 'react';
import './Card.css';

export default function Card({picture, name, city, toLeft, toRight}) {
  return <div className="card">
    <div className="info">
      <div className="thumb">
        <img src={picture} alt={picture}/>
      </div>
      <div className="name">{name}</div>
    </div>
    {city}
    <div className="nav">
      {toLeft && <div className="arrow" onClick={toLeft}>&#60;</div>}
      {toRight && <div className="arrow" onClick={toRight}>&#62;</div>}
    </div>
  </div>
}
